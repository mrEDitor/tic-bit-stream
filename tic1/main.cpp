#include <iostream>
#include <chrono>
#include <deque>
#include "bitset_ext.h"
#include "bit_stream.h"
#define N 16

int main()
{
	// typedef std::bitset<N + 1> carrier_t;
	typedef uint_fast32_t carrier_t;
	tic::BitStream<N, carrier_t> writer(true);

	auto started = std::chrono::steady_clock::now();
	for (int i = 0; i < 100'000; ++i)
	{
		std::deque<carrier_t> v;
		writer.writeStorageWith([&v](tic::BitStream<N, carrier_t>&b) -> void { v.emplace_back(b.readUnsafe(N / 2)); });
		writer.write(0b101010, 6);
		writer.write(0b101, 3);
		writer.write(0b10, 2);
		writer.write(0b00101011, 8);
		writer.write(0b0, 1);
		writer.write(0b1110111, 7);
		writer.write(0b0, 1);
		writer.write(0b1, 1);
		writer.flush();

		tic::BitStream<N, carrier_t> reader;
		reader.readStorageWith([&v](tic::BitStream<N, carrier_t>&b) -> void { b.writeClean(v.front(), N / 2); v.pop_front(); });
		/* std::cout << */ reader.read(6) /* << std::endl */;
		/* std::cout << */ reader.read(3) /* << std::endl */;
		/* std::cout << */ reader.read(2) /* << std::endl */;
		/* std::cout << */ reader.read(8) /* << std::endl */;
		/* std::cout << */ reader.read(1) /* << std::endl */;
		/* std::cout << */ reader.read(7) /* << std::endl */;
		/* std::cout << */ reader.read(1) /* << std::endl */;
		/* std::cout << */ reader.read(1) /* << std::endl */;
	}
	std::cout
		<< std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - started).count()
		<< " ms" << std::endl;

	return 0;
}
