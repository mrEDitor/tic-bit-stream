#pragma once
#include <bitset>

template <int bits>
std::bitset<bits> operator- (std::bitset<bits> l, int r)
{
	if (r != 1) throw std::exception("bad operand");
	if (l.count() != 1) throw std::exception("bad carrier");
	for (int i = 0; i < bits; ++i) {
		l.flip(i);
		if (!l.test(i)) break;
	}
	return l;
}
