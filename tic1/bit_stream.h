#pragma once
#include <algorithm>
#include <cassert>
#include <cstdint>
#include <fstream>
#include <functional>
#include <exception>
#include <iostream>

namespace tic
{
	/// <summary>
	/// Construct-time-sized buffered bit stream.
	/// </summary>
	/// <typeparam name="maxBits">
	/// Maximum bits to store.
	/// </typeparam>
	/// <typeparam name="carrier_t">
	/// Carrier defines underlying data type (and thus maximum buffer size.)
	/// Carrier type must define operations:
	/// carrier_t &lt;&lt; unsigned
	/// carrier_t &gt;&gt; unsigned
	/// carrier_t &amp; carrier_t
	/// carrier_t | carrier_t
	/// carrier_t - 1
	/// </typeparam>
	template <unsigned maxBits, typename carrier_t = uint_fast32_t>
	class BitStream
	{
		static constexpr carrier_t zero = 0;
		static constexpr carrier_t one = 1;
		static constexpr unsigned maxBytes = sizeof(carrier_t);
		static_assert(8 * maxBytes >= maxBits, "Unable to carry given maxBits, try to use another carrier_t");

	public:
		BitStream(bool flushPartial = false)
			: _flushPartial(flushPartial)
		{
			assert((one << 1) - 1 == one && "Corrupted carrier_t: (1<<1)-1 != 1");
			assert((zero & zero) == zero && "Corrupted carrier_t: 0&0 != 0");
			assert((zero & one) == zero && "Corrupted carrier_t: 0&1 != 0");
			assert((one & zero) == zero && "Corrupted carrier_t: 1&0 != 0");
			assert((one & one) == one && "Corrupted carrier_t: 1&1 != 1");
			assert((zero | zero) == zero && "Corrupted carrier_t: 0|0 != 0");
			assert((zero | one) == one && "Corrupted carrier_t: 0|1 != 1");
			assert((one | zero) == one && "Corrupted carrier_t: 1|0 != 1");
			assert((one | one) == one && "Corrupted carrier_t: 1|1 != 1");
			assert((zero ^ zero) == zero && "Corrupted carrier_t: 0^0 != 0");
			assert((zero ^ one) == one && "Corrupted carrier_t: 0^1 != 1");
			assert((one ^ zero) == one && "Corrupted carrier_t: 1^0 != 1");
			assert((one ^ one) == zero && "Corrupted carrier_t: 1^1 != 0");
		}

		/*********************
		 * TEST OPERATIONS
		 *********************/
		inline bool canRead(unsigned bits = 1) const { return _storedBits >= bits; }
		inline bool canWrite(unsigned bits = 1) const { return maxBits - _storedBits >= bits; }

		/*********************
		 * READING OPERATIONS
		 *********************/
		carrier_t read(unsigned bits = 1)
		{
			try 
			{
				if (bits > maxBits) throw BitStreamReadException("Requested word exceeds buffer size.");
				while (!canRead(bits)) _readStorage(*this);
				_storedBits -= bits;
				return (_data >> _storedBits) & mask(bits);
			}
			catch (BitStreamWriteException const&)
			{
				throw BitStreamReadException("Requested word exceeds available buffer size.");
			}
		}

		carrier_t readUnsafe(unsigned bits = 1)
		{
			try
			{
				if (bits > maxBits) throw BitStreamReadException("Requested word exceeds buffer size.");
				signed storedBits = _storedBits - bits;
				if (storedBits < 0 && !_flushPartial) throw BitStreamReadException("Partial reading attempt rejected.");
				_storedBits = (unsigned)std::max(storedBits, 0);
				if (storedBits >= 0) return (_data >> storedBits) & mask(bits);
				else return (_data << -storedBits) & (mask(bits) ^ mask(-storedBits));
			}
			catch (BitStreamWriteException const&)
			{
				throw BitStreamReadException("Requested word exceeds available buffer size.");
			}
		}

		/*********************
		 * PEEKING OPERATIONS
		 *********************/
		carrier_t peek(unsigned bits = 1)
		{
			try
			{
				if (bits > maxBits) throw BitStreamReadException("Requested word exceeds buffer size.");
				while (!canRead(bits)) _readStorage(*this);
				return _data >> (_storedBits - bits);
			}
			catch (BitStreamWriteException const&)
			{
				throw BitStreamReadException("Requested word exceeds available buffer size.");
			}
		}

		/*********************
		 * WRITING OPERATIONS
		 *********************/
		void write(carrier_t const& value, unsigned bits = 1) { writeClean(value & mask(bits), bits); }

		void writeClean(carrier_t const& value, unsigned bits = 1)
		{
			try
			{
				if (bits > maxBits) throw BitStreamWriteException("Passed word exceeds buffer size.");
				while (!canWrite(bits)) _writeStorage(*this);
				_storedBits += bits;
				_data = (_data << bits) | value;
			}
			catch (BitStreamReadException const&)
			{
				throw BitStreamWriteException("Passed word exceeds available buffer size (partial flush rejected).");
			}
		}

		/*****************************
		 * CARRIER TYPE MANIPULATIONS
		 ****************************/
		inline carrier_t mask(unsigned bits) { return bitAt(bits) - 1; }
		inline carrier_t bitAt(unsigned at) { return one << at; }

		inline carrier_t reverse(carrier_t value, unsigned bits)
		{
			for (auto i = 0; i < bits / 2; ++i)
			{
				auto j = bits - i - 1;
				auto v = ((value >> i) ^ (value >> j)) & one;
				value ^= (v << i) | (v << j);
			}
			return value;
		}

		/**********************
		 * DATA MISS CALLBACKS
		 **********************/
		void readStorageWith(std::function<void(BitStream<maxBits, carrier_t>&)> callback) { _readStorage = callback; }
		void writeStorageWith(std::function<void(BitStream<maxBits, carrier_t>&)> callback) { _writeStorage = callback; }
		void flush()
		{
			bool flushAnyway = true;
			std::swap(_flushPartial, flushAnyway);
			while (canRead()) _writeStorage(*this);
			std::swap(_flushPartial, flushAnyway);
		}

		struct BitStreamReadException : public std::runtime_error {
			BitStreamReadException(char const* msg) : std::runtime_error(msg) {}
		};
		struct BitStreamWriteException : public std::runtime_error {
			BitStreamWriteException(char const* msg) : std::runtime_error(msg) {}
		};

	private:
		unsigned _storedBits = 0;
		carrier_t _data = 0;
		bool _flushPartial;
		std::function<void(BitStream<maxBits, carrier_t>&)> _readStorage =
			[](BitStream<maxBits, carrier_t>&) -> void { throw BitStreamReadException("Storage reader is not binded."); };
		std::function<void(BitStream<maxBits, carrier_t>&)> _writeStorage =
			[](BitStream<maxBits, carrier_t>&) -> void { throw BitStreamWriteException("Storage writer is not binded."); };
	};
};
